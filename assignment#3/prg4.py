# prgrm to demo max,min n sum functions

_list1 = [0, 2, 4, -6, -8, 9, -15, 53, -59]
print(_list1)

print("\n the maximum number of the list is:\n", max(_list1))
print("\n the minimum number of the list is:\n", min(_list1))
print("\n the sum of the numbers of the list is:\n", sum(_list1))
