# prgrm to perform list append n extend operations
_l1 = ['Dhruval', 'Patel', 24, 3, 1995]

_l2 = ['GEC BVN', 'Info. Tech', 'python']

print("first list:", _l1)
print("\n second list:", _l2)

print("list 1 appended to list 2 results into:")
_l1.append(_l2)
print(_l1)

print("list 1 extended to list 2 results into:")
_l1.extend(_l2)
print(_l1)

