# prgrm to print sequence

# for loop
n = 5
for i in range(5, 27, 4):
    print(n)
    n += 4
# while loop
n = 5
while n < 27:
    print(n)
    n += 4
# without using any loop
print("using lists")
l = list(range(5, 27, 4))
print(l)
