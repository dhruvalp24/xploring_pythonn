# prgrm to demonstrate list comprehesion

planet = ['mercury', 'venus', 'earth', 'mars',
          'jupiter', 'saturn', 'uranus', 'neptune', 'pluto']
pl_name = input("enter the planet name")
if pl_name in planet[0:2]:
    print("well! its an inner planet")
elif pl_name in planet[2]:
    print("you are on our very own planet earth")
elif pl_name in planet[3:8]:
    print("well! its an outer planet")
elif pl_name in planet[8]:
        print("pluto is a dwarf planet!")
else:
        print("oops! it seems you have entered some other name....try again")
